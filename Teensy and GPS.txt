
#include <TinyGPS++.h>

TinyGPSPlus gps; // Definerer en TinyGPS type kalt gps

static void smartdelay(unsigned long ms);
void setup()
{
Serial.begin(9600); // Sett datahastighet til monitor
Serial1.begin(9600); // Sett datahastighet til GPSkontrollkort
}
void loop()
{
float flat, flon; // Definer variable for lengde- og breddegrad
int year; // Definer variable for år, dato og tid
byte month, day, hour, minute, second, hundredths;
unsigned long age; // Definer variabel for age

while (Serial1.available() > 0)
  gps.encode(Serial1.read());

//if (gps.altitude.isUpdated())
//  Serial.println(gps.altitude.meters());
  
Serial.print("LAT=");  Serial.println(gps.location.lat(), 6);
Serial.print("LONG="); Serial.println(gps.location.lng(), 6);
Serial.print("ALT=");  Serial.println(gps.altitude.meters());

Serial.print("Number of satellites in use   ");  Serial.println(gps.satellites.value()); // Number of satellites in use (u32)

Serial.println();
smartdelay(1000);
}
static void smartdelay(unsigned long ms)
{
unsigned long start = millis(); // Hent antall millisekunder siden reset av Arduino-kortet
do // Testen gjøres etter loopen er kjørt
{
while (Serial1.available())gps.encode(Serial1.read());
}
while (millis() - start < ms); // Testen gjøres før loopen er kjørt
}
